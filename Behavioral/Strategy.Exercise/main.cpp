#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>
#include <memory>
#include <tuple>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

using Data = std::vector<double>;
using Results = std::vector<StatResult>;

enum StatisticsType
{
	avg,
	min_max,
	sum
};

class Statistics
{
public:
	virtual ~Statistics() = default;
	virtual Results calculate(const Data& data) const = 0;
};

class Avg : public Statistics
{
	// Statistics interface
public:
	Results calculate(const Data& data) const override
	{
		double sum = std::accumulate(data.begin(), data.end(), 0.0);
		double avg = sum / data.size();

		return{ { "AVG", avg } };
	}
};

class MinMax : public Statistics
{
	// Statistics interface
public:
	Results calculate(const Data& data) const override
	{
		Data::const_iterator min_it, max_it;
		std::tie(min_it, max_it) = std::minmax_element(data.begin(), data.end());

		return { { "MIN", *min_it },{ "MAX", *max_it } };
	}
};

class Sum : public Statistics
{
	// Statistics interface
public:
	Results calculate(const Data& data) const override
	{
		double sum = std::accumulate(data.begin(), data.end(), 0.0);

		return{ { "SUM", sum } };
	}
};

using StatisticsPtr = std::shared_ptr<Statistics>;

class StatGroup : public Statistics
{
	std::vector<StatisticsPtr> stats_;
public:
	void add(StatisticsPtr stat)
	{
		stats_.push_back(stat);
	}

	Results calculate(const Data & data) const override
	{
		Results results;

		for (const auto& s : stats_)
		{
			auto stat_results = s->calculate(data);

			results.insert(results.end(),
				std::make_move_iterator(stat_results.begin()),
				std::make_move_iterator(stat_results.end()));			
		}

		return results;
	}
};

class DataAnalyzer
{
	StatisticsPtr statistics_;
	Data data_;
	Results results_;

public:
	DataAnalyzer(StatisticsPtr stat_type) : statistics_{ stat_type }
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();
		results_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

	void set_statistics(StatisticsPtr statistics)
	{
		statistics_ = statistics;
	}

	void calculate()
	{
		auto stat_results = statistics_->calculate(data_);

		results_.insert(results_.end(),
			std::make_move_iterator(stat_results.begin()),
			std::make_move_iterator(stat_results.end()));		
	}

	const Results& results() const
	{
		return results_;
	}
};

void show_results(const Results& results)
{
	for (const auto& rslt : results)
		std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
	auto avg = std::make_shared<Avg>();
	auto min_max = std::make_shared<MinMax>();
	auto sum = std::make_shared<Sum>();
	auto std_stats = std::make_shared<StatGroup>();
	std_stats->add(avg);
	std_stats->add(min_max);
	std_stats->add(sum);

	DataAnalyzer da{ std_stats };
	da.load_data("data.dat");
	da.calculate();	

	show_results(da.results());

	std::cout << "\n\n";

	da.load_data("new_data.dat");
	da.calculate();

	show_results(da.results());
}
