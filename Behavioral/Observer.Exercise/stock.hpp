#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <boost/signals2.hpp>

// Subject
class Stock
{
private:
	std::string symbol_;
	double price_;
	using PriceChangedEvent = boost::signals2::signal<void(std::string, double)>;
	using PriceChangedHandler = PriceChangedEvent::slot_type;

	PriceChangedEvent price_changed_;  // signal - event source
public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{
	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

	boost::signals2::connection connect(const PriceChangedHandler& handler)
	{
		return price_changed_.connect(handler);
	}

	// TODO: wyrejestrowanie obserwatora

	void set_price(double price)
	{
		if (price != price_)
		{
			price_ = price;

			price_changed_(symbol_, price_); // raising an event		
		}
	}
};

//class Observer
//{
//public:
//	virtual void update(/*...*/) = 0;
//	virtual ~Observer()
//	{
//	}
//};


class Investor 
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

	void update_porfolio(std::string symbol, double price)
	{
		std::cout << name_ << " has been notified: " << symbol << " - " << price << "$" << std::endl;
	}
};

#endif /*STOCK_HPP_*/
