#include "stock.hpp"

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
	Stock tpsa("TPSA", 95.0);

	// rejestracja inwestorow zainteresowanych powiadomieniami o zmianach kursu spolek
	Investor kulczyk_jr("Seba");
	Investor janusz("Byznesmen");

	auto conn1 = misys.connect([&kulczyk_jr](string symbol, double price) { kulczyk_jr.update_porfolio(symbol, price); });	
	tpsa.connect([&kulczyk_jr](string symbol, double price) { kulczyk_jr.update_porfolio(symbol, price); });
	auto conn2 = ibm.connect([&janusz](const auto& symbol, auto price) { janusz.update_porfolio(symbol, price); });

	// zmian kursow
	misys.set_price(360.0);
	ibm.set_price(210.0);
	tpsa.set_price(45.0);

	cout << "\n\n";

	conn1.disconnect();
	conn2.disconnect();

	misys.set_price(380.0);
	ibm.set_price(230.0);
	tpsa.set_price(15.0);
}
