#ifndef CHAIN_HPP_
#define CHAIN_HPP_

#include <iostream>
#include <memory>
#include <string>

// "Handler"
class Handler
{
protected:
    std::shared_ptr<Handler> successor_;

public:
    Handler() : successor_{nullptr} {}

    void set_successor(std::shared_ptr<Handler> successor)
    {
        successor_ = successor;
    }

	void handle_request(int request)
	{
		if (is_satisfied(request))
			process_request(request);
		else if (successor_ != nullptr)
			successor_->handle_request(request);
	}

    virtual ~Handler() = default;
private:
	virtual bool is_satisfied(int request) = 0;
	virtual void process_request(int request) = 0;
};

// "ConcreteHandler1"
class ConcreteHandler1 : public Handler
{
private:
	// Inherited via Handler
	virtual bool is_satisfied(int request) override;
	virtual void process_request(int request) override;
};

// "ConcreteHandler2"
class ConcreteHandler2 : public Handler
{
private:
	// Inherited via Handler
	virtual bool is_satisfied(int request) override;
	virtual void process_request(int request) override;
};

// "ConcreteHandler3"
class ConcreteHandler3 : public Handler
{
private:
	// Inherited via Handler
	virtual bool is_satisfied(int request) override;
	virtual void process_request(int request) override;
};

#endif /*CHAIN_HPP_*/
