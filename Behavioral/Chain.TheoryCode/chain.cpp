#include "chain.hpp"

bool ConcreteHandler1::is_satisfied(int request)
{
	return (request >= 0) && (request < 10);
}

void ConcreteHandler1::process_request(int request)
{
	std::cout << "ConcreteHandler1 handled request " << request << std::endl;
}

bool ConcreteHandler2::is_satisfied(int request)
{
	return (request >= 10) && (request < 20);
}

void ConcreteHandler2::process_request(int request)
{
	std::cout << "ConcreteHandler2 handled request " << request << std::endl;
}

bool ConcreteHandler3::is_satisfied(int request)
{
	return (request >= 20) && (request < 30);
}

void ConcreteHandler3::process_request(int request)
{
	std::cout << "ConcreteHandler3 handled request " << request << std::endl;
}
