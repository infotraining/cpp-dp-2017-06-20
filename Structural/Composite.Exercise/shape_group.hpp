#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <memory>
#include <vector>

#include "shape.hpp"

namespace Drawing
{
    class ShapeGroup : public CloneableShape<ShapeGroup>
    {
		std::vector<std::unique_ptr<Shape>> shapes_;
	public:
		using iterator = std::vector<std::unique_ptr<Shape>>::iterator;
		using const_iterator = std::vector<std::unique_ptr<Shape>>::const_iterator;

		constexpr static auto id = "ShapeGroup";

		ShapeGroup() = default;

		ShapeGroup(const ShapeGroup& src)
		{
			for (const auto& s : src.shapes_)
				shapes_.push_back(s->clone());
		}

		ShapeGroup& operator=(const ShapeGroup& src)
		{
			ShapeGroup temp(src);
			swap(temp);

			return *this;
		}

		ShapeGroup(ShapeGroup&&) = default;
		ShapeGroup& operator=(ShapeGroup&&) = default;

		void swap(ShapeGroup& other)
		{
			shapes_.swap(other.shapes_);
		}

		// Inherited via Shape
		virtual void move(int dx, int dy) override;
		virtual void draw() const override;

		void add(std::unique_ptr<Shape> s)
		{
			shapes_.push_back(std::move(s));
		}

		size_t size() const
		{
			return shapes_.size();
		}

		iterator begin()
		{
			return shapes_.begin();
		}

		const_iterator begin() const
		{
			return shapes_.begin();
		}

		iterator end()
		{
			return shapes_.end();
		}

		const_iterator end() const
		{
			return shapes_.end();
		}
	};
}

#endif // SHAPEGROUP_HPP
