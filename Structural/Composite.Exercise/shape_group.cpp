#include "shape_group.hpp"
#include "shape_factories.hpp"
#include <algorithm>

using namespace std;
using namespace Drawing;

namespace
{
	bool is_registered =
		SingletonShapeFactory::instance()
			.register_creator(ShapeGroup::id, &make_unique<ShapeGroup>);
}

void Drawing::ShapeGroup::move(int dx, int dy)
{
	for (const auto& s : shapes_)
		s->move(dx, dy);
}

void Drawing::ShapeGroup::draw() const
{
	for (const auto& s : shapes_)
		s->draw();
}
