#ifndef TEXT_HPP
#define TEXT_HPP

#include "paragraph.hpp"
#include "shape.hpp"
#include <string>

namespace Drawing
{

    // TODO - zaadaptowac klase Paragraph do wymogow klienta
    class Text : public ShapeBase, private LegacyCode::Paragraph
    {
	public:
		constexpr static auto id = "Text";

		Text(int x = 0, int y = 0, const std::string& text = "") 
			: ShapeBase{x, y}, LegacyCode::Paragraph(text.c_str())
		{}

		// Inherited via ShapeBase
		virtual void draw() const override;

		std::string text() const
		{
			return get_paragraph();
		}

		void set_text(const std::string& text)
		{
			set_paragraph(text.c_str());
		}
	};
}

#endif
