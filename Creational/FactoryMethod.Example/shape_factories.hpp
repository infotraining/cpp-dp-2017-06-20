#pragma once
#include <typeindex>
#include <functional>
#include <unordered_map>
#include <string>
#include <mutex>

#include "shape.hpp"
#include "shape_readers_writers\shape_reader_writer.hpp"

template <
	typename ProductT,
	typename KeyT = std::string,
	typename CreatorT = std::function<std::unique_ptr<ProductT>()>>
class GenericFactory
{
	std::unordered_map<KeyT, CreatorT> creators_;
public:
	bool register_creator(const KeyT& id, const CreatorT& creator)
	{
		return creators_.insert(std::make_pair(id, creator)).second;
	}

	std::unique_ptr<ProductT> create(const KeyT& id)
	{
		auto& creator = creators_.at(id);
		return creator();
	}
};

using ShapeFactory = GenericFactory<Drawing::Shape>;
using ShapeRWFactory = GenericFactory<Drawing::IO::ShapeReaderWriter, std::type_index>;

template <typename T>
std::type_index make_type_index(const T& obj)
{
	return std::type_index{ typeid(obj) };
}

template <typename T>
std::type_index make_type_index()
{
	return std::type_index{ typeid(T) };
}

template <typename T>
class SingletonHolder
{
private:
	SingletonHolder() = default;	
public:
	SingletonHolder(const SingletonHolder&) = delete;
	SingletonHolder& operator=(const SingletonHolder&) = delete;

	static T& instance()
	{
		static T unique_instance;

		return unique_instance;
	}

	//static T& bad_instance()
	//{
	//	if (instance_.load() == nullptr)
	//	{
	//		std::lock_guard<std::mutex> lk{ mtx_ };

	//		if (instance_ == nullptr)
	//		{
	//			// 1 - malloc
	//			void* raw_mem = operator new(sizeof(T));

	//			// 2 - construct
	//			new (raw_mem) T();

	//			// 3 - assignement
	//			instance_.store(static_cast<T*>(raw_mem));
	//		}
	//	}

	//	return instance_;
	//}
};

using SingletonShapeFactory = SingletonHolder<ShapeFactory>;