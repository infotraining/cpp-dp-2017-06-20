#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "factory.hpp"

using namespace std;

class Client
{	
    Modern::ServiceCreator creator_;

public:
    Client(Modern::ServiceCreator creator) : creator_(creator)
    {
    }

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
    {
        unique_ptr<Service> service = creator_();

        string result = service->run();
        cout << "Client is using: " << result << endl;
    }
};

typedef std::map<std::string, Modern::ServiceCreator > Factory;

int main()
{
    Factory creators;
	creators.insert(make_pair("ServiceA", [] { return make_unique<ConcreteServiceA>(); }));
    creators.insert(make_pair("ServiceB", &make_unique<ConcreteServiceB>));
	creators.insert(make_pair("ServiceC", &make_unique<ConcreteServiceC>));

    Client client(creators["ServiceC"]);
    client.use();

	vector<int> vec = { 1, 2, 3 };

	for (const auto& item : vec)
	{
		cout << item << "\n";
	}

	for (auto it = vec.begin(); it != vec.end(); ++it)
	{
		const auto& item = *it;
	}
}
