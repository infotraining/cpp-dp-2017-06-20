#include "circle_reader_writer.hpp"
#include "../circle.hpp"
#include "../shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

// TODO - register CircleReaderWriter creator in a factory
namespace
{
	using namespace std;
	using namespace Drawing;

	bool is_registered =
		SingletonShapeRWFactory::instance()
			.register_creator(make_type_index<Circle>(), &make_unique<CircleReaderWriter>);

}
