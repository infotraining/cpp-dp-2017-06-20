#include <cassert>
#include <iostream>
#include <memory>
#include <typeinfo>
#include <cassert>
#include <gsl.h>

using namespace std;

class Engine
{
public:
	virtual void start() = 0;
	virtual void stop() = 0;

	std::unique_ptr<Engine> clone() const
	{
		auto cloned_engine = do_clone();
		assert(typeid(*cloned_engine) == typeid(*this));

		return cloned_engine;
	}

    virtual ~Engine() = default;
private:
	virtual std::unique_ptr<Engine> do_clone() const = 0;
};

// CRTP
template <typename EngineT, typename EngineBase = Engine>
class CloneableEngine : public EngineBase
{
private:
	std::unique_ptr<Engine> do_clone() const
	{
		return std::make_unique<EngineT>(static_cast<const EngineT&>(*this));
	}
};

class Diesel : public CloneableEngine<Diesel>
{
public:
    virtual void start() override
    {
        cout << "Diesel starts\n";
    }

    virtual void stop() override
    {
        cout << "Diesel stops\n";
    }
};

class TDI : public Diesel
{
public:
    virtual void start() override
    {
        cout << "TDI starts\n";
    }

    virtual void stop() override
    {
        cout << "TDI stops\n";
    }
};

class Hybrid : public CloneableEngine<Hybrid>
{
public:
    virtual void start() override
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop() override
    {
        cout << "Hybrid stops\n";
    }
};

class Car
{
    unique_ptr<Engine> engine_;

public:
    Car(unique_ptr<Engine> engine) : engine_{ move(engine) }
    {
    }

	Car(const Car& src) : engine_{ src.engine_->clone() }
	{
	}

	Car& operator=(const Car& src)
	{
		if (this != &src)
		{
			engine_ = src.engine_->clone();
		}

		return *this;
	}

	Car(Car&&) = default;
	Car& operator=(Car&&) = default;

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
	/*gsl::owner<Engine*> e = new TDI();

	delete e;*/

    Car c1{ make_unique<TDI>() };
    c1.drive(100);

    cout << "\n";

	Car c2 = c1;
	c2.drive(200);
}
