#include "game.hpp"

using namespace std;
using namespace Game;

int main()
{
    GameApp game{std::make_unique<EasyLevelEnemyFactory>()};
    game.init_game(12);
    game.play();

    cout << "\n\n";

    game.select_level(GameLevel::die_hard);
    game.init_game(12);
    game.play();
}
